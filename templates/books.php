<?php 
//fetch all books from library
global $wpdb;
$library_table = $wpdb->prefix."wygp_library";

function generate_search_criteria($search_words,$criteria) {
    $s_terms = $search_words;
    $n = count($s_terms);
    $search_c = '';
    for($i = 0; $i < $n; $i++) {
        if( ($i+1) < $n )
            $search_c .= "$criteria LIKE '%{$s_terms[$i]}%' OR ";
        else $search_c .= "$criteria LIKE '%{$s_terms[$i]}%'";
    }
    return $search_c;
}

if(isset($_POST['search-lib'])) {
    $search_words = $_REQUEST['search-words'];
    $search_words_array = explode(' ',$search_words);
    $criteria = $_REQUEST['criteria'];
    switch($criteria) {
        case 'book-title':
            $search_criteria = generate_search_criteria($search_words_array,'book_name');//"book_name LIKE '%$search_words%'";
            //die($search_criteria);
        break;
        case 'book-author':
        $search_criteria = generate_search_criteria($search_words_array,'book_author');//"book_author LIKE '%$search_words%'";
        //die($search_criteria);
        break;
        case 'year':
        $search_criteria = "published_year LIKE '%$search_words%'";
        break;
    }
    $result = $wpdb->get_results("SELECT book_name, book_cover, book_author, post_id FROM $library_table WHERE $search_criteria",OBJECT);
    //die("search received! $search_criteria, $search_words ");
    $list_title = "<h4>Search Result for: $search_words </h4>";
}
else {
    $result = $wpdb->get_results("SELECT book_name, book_cover, book_author, post_id FROM $library_table",OBJECT);
    $list_title = "<h4>New Release</h4>";
}

?>
<div class="main-book-wrapper">
    <?php include_once dirname(__FILE__)."/sidebar.php"; ?>
    <div class="book-wrapper">
        <div class="book-header">
            <div id='wygp-search-wrapper'>
                <form action = "<?php echo site_url().'/books'; ?>" method="post">
                    <input type="hidden" name="action" value ="search_library">
                    <input type="hidden" name="data" value="The post would be available here">
                    <input type="text" placeholder = 'Search Library' name= 'search-words'> <input type="submit" value='Search' name = 'search-lib'>
                    <label>
                        Title<input type="radio" name="criteria" value='book-title'>
                    </label>
                    <label> Author <input type="radio" name="criteria" value ='book-author'></label>
                    <label>Year<input type="radio" name="criteria" value='year'></label>
                </form>
            </div>
            <?php echo $list_title; ?>
        </div>
        <div class="book-content-wrapper">
            <?php 

                $book_list = "<div class='book-list-wrapper'>";
                $base_url = site_url()."/details/?wygp=";
                foreach($result as $index=>$book) { 
                    $found = true;
                    $book_list .= "
                    <div class = 'book'>
                        <a href='$base_url$book->post_id'><img src = '$book->book_cover' alt='book cover' id='book-cover'></a>
                        <h5 id='book-title'>$book->book_name</h3>
                        <p id='book-author'>By: $book->book_author</p>
                        <p style='color:red' id = 'book-rating'> Rating: 4 star</p>
                    </div>
                    ";
                }
                if($found)
                    $book_list .= "</div>";
                else $book_list .= "<p style='margin-top:30%; margin-left:auto; margin-right:auto; margin-bottom:30%;'>No Result found for: $search_words </p></div>";
                echo $book_list;
            ?>
        </div>
    </div>
</div>