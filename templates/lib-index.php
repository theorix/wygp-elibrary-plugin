<?php 
//fetch all books from library
global $wpdb;
$library_table = $wpdb->prefix."wygp_library";

$result = $wpdb->get_results("SELECT DISTINCT published_date FROM $library_table ORDER BY published_date DESC",OBJECT);
$base_url = site_url()."/books/?lib-index=";
?>
<div class="main-book-wrapper">
    <?php include_once dirname(__FILE__)."/sidebar.php"; ?>
    <div class="book-wrapper">
        <div class="book-header">
            <h4>Book Index</h4>
        </div>
        <div class="book-content-wrapper">
            <?php 
            $index_list = "<div class='book-list-wrapper'><div class='book'><ul>";
            foreach($result as $i=>$pub_date) {
                $date_ = explode('-',$pub_date->published_date); 
                $date_ = $date_[0];
                $index_list .= "<li><a href='$base_url$date_'>$date_</a></li>";
            }
            $index_list .= "</ul></div></div>";
            echo $index_list;
            ?>
        </div>
    </div>
</div>