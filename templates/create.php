<div class="add-container">
  <div class="add-box" id='add-box-container'>
      <h3 style="color:green" id="msg"  ></h3>
      <form action = "<?php echo site_url().'/wp-admin/admin-post.php'; ?>" method='post' id="f">
        <input type="hidden" name="action" value ="add_book">
        <input type="hidden" name="data" value="The post would be available here">

        <div class="info">
          <h4>Add New Book</h4>
          <p>
            Add a new book to the library.
          </p>
        </div>
        
        <div id ='page1'>
          <div class="form-group"> 
            <input type="text" placeholder="Book Name" name="book-name" id="book-name">
          </div>
          <div class="form-group">
            <input type="text" name="book-author" placeholder = 'Author' id="book-author">
          </div>
          <div class="form-group">
            <input type="text" name="book-price" placeholder = 'Price' id="book-price">
          </div>
          <div class="form-group">
            <label for='pub-year'>Date of Publication</label><br/>
            <input type="date" name="pub-year" id="pub-year">
          </div>
        </div>
        
        <div id='page2'>
          <div class="form-group">
            <label for="description">Book Description</label>
            <textarea placeholder="Description" name="description" id="description"></textarea>
          </div>

          <div class="form-group">
            <!--<label for="book-cat">Book Category</label> -->
            <select name="book-cat" id="book-cat">
              <option value = "select">Select Book Category</option>
              <option value = "politics">Politics</option>
              <option value = "social">Social</option>
              <option value = "IT">Information Tech</option>
            </select>
          </div>
          <div class="form-group">
            <!--<label for="book-fee">Book Type</label> -->
            <select name="book-fee" id="book-fee">
              <option value = "select">Select Book type</option>
              <option value = "free">Free</option>
              <option value = "paid">Paid</option>
            </select>
          </div>
        </div>
        
        <div id='page3'>
          <div class="form-group">
            <label for="image_url">Book Cover Image</label>
            <input type="hidden" name="image_url" id="image_url" class="regular-text">
            <input type="button" name="upload-btn" id="upload-btn" class="button-secondary" value="Upload Image">
            
            <input type="hidden" name="pdf_url" id="pdf_url" class="regular-text">
            <input type="button" name="upload-btn2" id="upload-btn2" class="button-secondary" value="Upload PDF Book">
            <img src="http://localhost/theorix/wp-content/uploads/2018/07/waterfallModel.png" alt="Course Logo" id ="logo" name = 'logo' class="cover-img"/>
          </div>
          <button id = "submit">Add Book</button>
        </div>
      </form>
  </div>

</div>




<?php // jQuery
wp_enqueue_script('jquery');
// This will enqueue the Media Uploader script
wp_enqueue_media();

wp_enqueue_script("activity",plugin_dir_url(__FILE__)."$plugin_name/activity/activity.js");
?>
  
<script type="text/javascript">
//create a function to temporary show the the image
function show_logo(img_src) {
  document.getElementById("logo").src = img_src;
}

jQuery(document).ready(function($){
    $('#upload-btn').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // We convert uploaded_image to a JSON object to make accessing it easier
            // Output to the console uploaded_image
            //console.log(uploaded_image);
            var image_url = uploaded_image.toJSON().url;
            // Let's assign the url value to the input field
            $('#image_url').val(image_url);

            //show logo
            show_logo(image_url);
        });
    });


    $('#upload-btn2').click(function(e) {
        e.preventDefault();
        var book = wp.media({ 
            title: 'Upload Book',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_book = book.state().get('selection').first();
            var book_url = uploaded_book.toJSON().url;
            console.log(uploaded_book.toJSON().filename);
            filename = uploaded_book.toJSON().filename;
            file_ext = filename.substr(filename.length - 3,3);
            console.log(file_ext);
            
            if(file_ext === 'pdf')
            {
              $('#pdf_url').val(book_url);  // Let's assign the url value to the input field
              //console.log("book url",document.getElementById('pdf_url').value);
            }
            else {  //TODO: display an alert here
              $("#book_url").val("/");
              console.log("file "+book_url," is not a pdf file");
            }

            //show logo
            //show_logo(image_url);
        });
    });
});


//user flow activity
let user_flow = new Activity({container:"f", nextBtn:true,prevBtn:true});
</script>