<div class="main-book-wrapper">
    <?php include_once dirname(__FILE__)."/sidebar.php"; ?>
    <div class="book-wrapper">
        <div class="book-header">
            <h4>New Release</h4>
        </div>
        <div class="book-content-wrapper">
            <div class = 'book-detail-info'>
                <?php 
                    //fetch all books from library
                    $post_id = isset($_GET['wygp'])?$_GET['wygp']:"0";
                    global $wpdb;
                    $library_table = $wpdb->prefix."wygp_library";
                    $result = $wpdb->get_results("SELECT book_name, book_cover, book_author, book_url, post_id FROM $library_table WHERE post_id = $post_id",OBJECT);
                    //die(print_r($result));
                    $book = $result[0];
                    $details = "
                    <img src = '$book->book_cover' alt='book cover' id='book-detail-cover'>
                    <h3 id='book-title'>$book->book_name</h3>
                    <p id='bk-author' style='color:red'>By: $book->book_author</p>
                    ";
                    //$details .= "<div id = 'option-btn'><button id = 'read-now' onclick = ". "read_now('$book->book_url')".">READ NOW </button> <a href='$book->book_url' download>Download</a></div>";
                    $details .= "<div id = 'option-btn'><a href='$book->book_url' target='_blank' id = 'read-now' onclick = ". "read_now('$book->book_url')".">READ NOW </a> <a id = 'downl' href='$book->book_url' download>Download</a></div>";
                    echo $details;

                ?>
            </div>

            <div class="book-desc">
                <?php 
                
                    $books = new WP_Query(array("post_type"=>"book", 'p'=>$post_id));

                    if($books->have_posts()) {
                        //die("There is posts");
                        $books = $books->posts;
                        foreach($books as $index=>$b) {
                             echo "<p id='book-content-desc'>$b->post_content</p>";
                        }
                    }
                ?>
            </div>
            
        </div>
    </div>
</div>