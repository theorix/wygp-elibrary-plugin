<?php

/*
*   Plugin Name: WYGP E-Library
*   Description: This plugin is the E - library module for the WYGP system
*   Author: Israel Duff
*   Version: 0.0.1
*/

require_once dirname(__FILE__)."/library.php";
register_activation_hook(__FILE__,array("Library","plugin_activated"));
register_deactivation_hook(__FILE__,array("Library","plugin_deactivated"));


/*add_filter( 'sidebars_widgets', 'crunchify_code_disable_widgets' );
function crunchify_code_disable_widgets( $sidebars_widgets ) { //die("I executed");
        if (is_page(array(1453,1447,1448,1455))) // replace this with post/page ID
                $sidebars_widgets = array( false );
                return $sidebars_widgets;
}
*/

?>
