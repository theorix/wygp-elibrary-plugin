/* activity like code in js */
function Activity(options) {
  //this.views = views; //console.log("constructed",this.views)
  let view_stack = [];
  let next_condition_views = [];
  let prev_condition_views = [];
  let slide_show = false;
  let g_effect = {display:"block",transition:"all 3s ",opacity:"1",marginLeft:"0"};    //global style effect to be applied to views
  let g_no_effect = {display:"none"};
  this.current_pos = 0;


  /*
  * Internal private functions use to manipulate internal states
  */

//function to find all views in the container and populate the views array
  find_children = function(container) {
    let parent_container = document.getElementById(container); //console.log(parent_container.classList);
    let ignored_pages = document.getElementsByClassName('activity-ignore');
    let ignore_page = false;
    //add the style to the parent
    parent_container.classList.add("activity-container")
    let views = [], j = 0;
    if(parent_container) {
      for(let i = 0; i < parent_container.childNodes.length; i++) {
        if(parent_container.childNodes[i].id) { //console.log("rule: ",i);
          for(let k = 0; k < ignored_pages.length; k++) {
            if(ignored_pages[k].id && ignored_pages[k].id === parent_container.childNodes[i].id) {
              ignore_page = true;
              break;
            }
          }
          if(!ignore_page) views[j++] = parent_container.childNodes[i].id;
          ignore_page = false;
        }
      }
    }
    //console.log("found views:",views);
    return views;
  }

//functions for adding/removing effects to the views
  function apply_effect(view_id,effect) {
    let eff;
    let element = document.getElementById(view_id);

    if(!effect) { //apply the global effect
      eff = g_effect;
    }
    else eff = effect;

    for(prop in eff) { //console.log(prop);
      element.style[prop] = eff[prop];
    }

    /*these line of code are the tricks to let css transition effect work
    * you first set the opacity to '0', at the very beggining... then afterwards
    * you then cause the browser to stop the render process by calling offsetHeight
    * Then finally, you set back the opacity to 1.
    */
    element.offsetHeight;
    //element.style.opacity = "0";
    //element.style.marginLeft = "50%";

  }

  function remove_effect(view_id,effect) {
    //document.getElementById(view_id).style.display = "none";
    if(!effect)
      apply_effect(view_id,g_no_effect);
    else apply_effect(view_id,effect);
  }


//populate the views array with elements
 this.views = find_children(options.container);

  function createNavBtn(name) {
    let btn = document.createElement('a');
    btn.id = name+"Btn";
    btn.href="#";
    btn.style.display="none";
    btn.innerHTML = name;
    return btn;
  }

  //determine whether or not to create navigation buttons
  this.nextBtn = createNavBtn('Next');
  this.prevBtn = createNavBtn("Previous");
  this.nextBtn.style.marginLeft="70%";
  //create a wrapper div arround the nav buttons
  let btn_wrapper = document.createElement('div');
  btn_wrapper.style.width = "100%";
  //btn_wrapper.style.backgroundColor="grey";

  //add the nav buttons to the wrapper
  btn_wrapper.appendChild(this.prevBtn);
  btn_wrapper.appendChild(this.nextBtn);
  document.getElementById(options.container).appendChild(btn_wrapper);
  //document.getElementById(options.container).appendChild(this.prevBtn);
  //document.getElementById(options.container).appendChild(this.nextBtn);

    if(options.nextBtn) {
      this.nextBtn.style.display = "inline"
    }
    if(options.prevBtn) {
      this.prevBtn.style.display = "inline";
      //console.log(document.getElementById(options.container))
    }

  let that = this;

  for(let i = 1; i < this.views.length; i++) { //console.log(i);
    document.getElementById(this.views[i]).style.display="none";
  }

  /*
  * Some methods to manage states
  */

  function get_state() {
    let state = {position:that.current_pos};
    that.nextBtn?(state['nextBtn']=true):state['nextBtn']=false;
    that.prevBtn?(state['prevBtn']=true):state['prevBtn']=false;
    return state;
  }

  function restore_state(state) {
    if(!state) return;
    that.current_pos = state['position'];
    if(state['nextBtn']) that.nextBtn.style.display="inline";
    if(state['prevBtn']) that.prevBtn.style.display="inline";
  }


  this.nextView = function(click_evt,view_id) {
    click_evt.preventDefault(); //prevent form from submitting
    //check for conditional next on this current view
    if(next_condition_views[that.views[that.current_pos]]) { //console.log("this.current_pos",view_id)
      //view_stack.push(that.current_pos);
      if(!slide_show) view_stack.push(get_state());
      //document.getElementById(that.views[that.current_pos]).style.display="none";
      remove_effect(that.views[that.current_pos])

      let cview = next_condition_views[that.views[that.current_pos]];

      //get current view obj
      cview_obj = that.get_view_obj(that.views[that.current_pos]);

      //define a get method for the current view
      cview_obj.get = function (id) {
        if(!this[id]) return undefined;
        //check for radio buttons and checkbox -- return undefined if unchecked else return value
       if(this[id].type === 'radio' || this[id].type === 'checkbox') {
            if(this[id].checked === true) return this[id].value;
            else return undefined;
        }

        if(this[id].type === 'textarea') { //console.log("text area get")
          return this[id].innerHTML;
        }

        return this[id].value;
      };

      //define a set method for the current view object
      cview_obj.set = function (id,value) { //console.log(id,"value:",this[id]);
          if(this[id])
            that.set(id,value);
      };

      //determine the next view to display by using the callback to()
      let next_view = cview.to(cview_obj);

      //check the value returned by to()
      if(typeof next_view === 'string') {//since nothing was specified - enable all features
        that.nextBtn.style.display = "inline";
        that.prevBtn.style.display = "inline";
        that.current_pos = that.views.indexOf(next_view);
      }
      else if(typeof next_view === 'object') { //check what features are allowed
        if(!next_view['nextBtn']) that.nextBtn.style.display = "none";
        if(!next_view['prevBtn']) that.prevBtn.style.display = "none";
        that.current_pos = that.views.indexOf(next_view['nextID']);
      }
      //document.getElementById(that.views[that.current_pos]).style.display="block";
      apply_effect(that.views[that.current_pos])
    }
    else {
      if(!view_id && that.current_pos < (that.views.length - 1)) {
        //view_stack.push(that.current_pos);
        if(!slide_show) view_stack.push(get_state());
        remove_effect(that.views[that.current_pos]);
        apply_effect(that.views[++that.current_pos]);
      }
      else if(view_id < that.views.length){
        //view_stack.push(view_id);
        if(!slide_show) {
          let state = get_state();
          state['position'] = view_id;
          view_stack.push(state);
        }
        //document.getElementById(that.views[that.current_pos]).style.display="none";
        remove_effect(that.views[that.current_pos]);
        that.current_pos = view_id;
        //document.getElementById(that.views[view_id]).style.display="block";
        apply_effect(that.views[that.current_pos]);
      }
    }
  };

  this.prevView = function(evt) {
    evt.preventDefault();
    if(view_stack.length > 0) {
      let prevState = view_stack.pop();
      remove_effect(that.views[that.current_pos]);
      apply_effect(that.views[prevState['position']]);
      restore_state(prevState);
    }

    //if(that.current_pos == 0) that.nextBtn.style.display="none";
  };

  this.get = function(attr_id) {
    //get the dom of the current visible view and return the value of
    return document.getElementById(attr_id).value;
  }

  this.set = function(attr_id,value) {
    //if(attr_id === 'adeditor') console.log("setting TextEditor ",attr_id)
    let target = document.getElementById(attr_id);
    if(target.nodeName !== "SELECT" && target.nodeName !== 'INPUT') {
      target.innerHTML = value;
    }
    else target.value = value;
  }


  this.addNext = function(from,to) {
    let newNext = {from:from, to:to};
    next_condition_views[from] = newNext;
  }

  this.addPrev = function(from,to){
    //this.addNext(from,to);
    let newNext = {from:from, to:to};
    prev_condition_views[from] = newNext;
  }

  this.find_child = function(parent_view,element_id) {
    let element = document.getElementById(parent_view);
    return element.querySelector(`[id=${element_id}]`);
  }



  this.get_view_obj = function(view_id,partial_obj,no_id) {
    var view_obj, baseElement;
    if(partial_obj) view_obj = partial_obj;
    else view_obj = {};

    if(view_id) {
      if(no_id) {
        //use the provided view_id as the base element
          baseElement = view_id;
      }
      else {
        //use the getElement method to get the base element
        baseElement = document.getElementById(view_id);
      }
    }
    else return view_obj;

    let target = '';
    for(let i = 0; i < baseElement.childNodes.length; i++) {
      //get all IDs of these element
      target = baseElement.childNodes[i].nodeName; //console.log("baser:",target)
      if(target === 'INPUT' || target === 'SELECT' || target === "TEXTAREA") {
        //console.log("baser:",baseElement.childNodes[i].nodeName)
        view_obj[baseElement.childNodes[i].id] = baseElement.childNodes[i];
      }
      else {
          if(!baseElement.childNodes[i].id)
            view_obj =  that.get_view_obj(baseElement.childNodes[i],view_obj,'no id');
          else view_obj = that.get_view_obj(baseElement.childNodes[i].id,view_obj);
      }
    }
    return view_obj;
  }


  //add auto play function
  this.auto_play = function(opt) { console.log(that.current_pos);

    setTimeout(function() {//console.log("auto playing...");
    if(that.current_pos === that.views.length -1){
      that.nextView(null,0);
    }
    else  that.nextView();
      that.auto_play(opt);
    },opt.delay);
  }

  //document.getElementById(this.nextBtn).addEventListener('click', that.nextView);
  //document.getElementById(this.prevBtn).addEventListener('click', that.prevView);
  this.nextBtn.addEventListener('click', that.nextView);
  this.prevBtn.addEventListener('click', that.prevView);
}

//notify when user tries to reload the page
window.onbeforeunload = function (e) {
    var e = e || window.event;

    // For IE and Firefox
    if (e) {
        e.returnValue = 'Leaving the page';
    }

    // For Safari
    return 'Leaving the page';
};
