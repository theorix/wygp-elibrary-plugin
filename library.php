<?php 
class Library {
    /*
    * The library constructor
    */
    function __construct() { 
        //create book custom post type at init
        add_action("init",array($this,"create_post_type"));

        //add user capacity for media upload purposes
        add_action("init",array($this,"user_capacity"));
        add_action("admin_init",array($this,"user_capacity"));
        add_action("admin_post_add_book",array($this,"add_book"));
        add_action("admin_post_search_library",array($this,"search_library"));

        //filters
        add_filter("ajax_query_attachments_args",array($this,"show_current_user_attachments"));


        //short codes here
        add_shortcode("wygp_add_book",array($this,"add_book_page"));
        add_shortcode("wygp_show_books",array($this,"show_books"));
        add_shortcode("wygp_show_book",array($this,"show_book"));
        add_shortcode("wygp_library_index",array($this,"show_index"));


        //styles and scripts
        wp_enqueue_style("library-style",plugin_dir_url(__FILE__)."$plugin_name/library-style.css");
        //wp_enqueue_style("activity",plugin_dir_url(__FILE__)."$plugin_name/activity/activity.css");
        wp_enqueue_script("elibrary",plugin_dir_url(__FILE__)."$plugin_name/elibrary.js");
        
        

    }

    /* 
    *   Library method to create neccessary pages
    */
    function library_pages() {
        return array(
            array(
                "title"=>"Books",
                "content"=>"[wygp_show_books]",
                "name"=>"books"
            ),
            array(
                "title"=>"Book Details",
                "content"=>"[wygp_show_book]",
                "name"=>"details"
            ),
            array(
                "title"=>"Add Book",
                "content"=>"[wygp_add_book]",
                "name"=>"add-book"
            ),
            array(
                "title"=>"Index",
                "content"=>"[wygp_library_index]",
                "name"=>"library-index"
            )
        );
    }

    
    /*
    *   Plugin activation and deaction functions
    */
    function plugin_activated() { 

        global $wpdb;

        //use the wp database prefix defined
        $library_table = $wpdb->prefix."wygp_library";

        //get charset collate
        $char_set_collate = $wpdb->get_charset_collate();

        //write your sql query to create table
        $query = "CREATE TABLE IF NOT EXISTS $library_table (
        id mediumint(10) NOT NULL AUTO_INCREMENT,
        book_name varchar(100) DEFAULT '' NOT NULL,
        published_date date,
        book_cover varchar(255) DEFAULT '',
        book_url varchar(255) DEFAULT '',
        book_fee varchar(50) DEFAULT 'free',
        price int(20) DEFAULT 0,
        book_author varchar(255) DEFAULT 'No Author',
        book_desc varchar(255) DEFAULT 'No description',
        post_id int(50),
        PRIMARY KEY (id)
        ) $char_set_collate;
        ";

        
       // die($query3);

        //execute the query using the db delta function
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( $query );
        


        $library_pages = Library::library_pages(); //die(print_r($lms_admin_pages));
  
      foreach($library_pages as $slug=>$page) {
        //use wordpress query class
        $query = new WP_Query("pagename=".$page['name']);
        if(!$query->has_posts()) { //   !is_page($page['name'])
          $page_id = wp_insert_post(array(
            "post_title"=>$page['title'],
            "post_name"=>$page['name'],
            "post_type"=>"page",
            "post_content"=>$page['content'],
            "post_status"=>"publish",
            "ping_status"=>"closed",
          ));
  
          //store the page id as wp options
          add_option($page['name'],$page_id);
          //add_option("wygp_news_page".$slug,$page_id);
        }
      }

      //increase media upload filesize
      @ini_set("upload_max_size","10M");
      @ini_set('post_max_size',"10M");
      @ini_set('max_execution_time','300');

    }

    function plugin_deactivated() {
      $library_pages = Library::library_pages();
      foreach($library_pages as $slug=>$page) {
        $page_id = get_option($page['name']);
        wp_delete_post($page_id,true);
        delete_option($page['name']);
      }
    }

    function get_template_html( $template_name, $attributes = null ) {
        if ( ! $attributes ) {
            $attributes = array();
        }
    
        ob_start();   //i.e start recording...
        require( 'templates/' . $template_name . '.php'); // include the file -- (its now in buffer)
        $html = ob_get_contents();      //get the contents of the file in buffer
        ob_end_clean(); // since done copying from buffer, delete the contents
    
        return $html;
    }

    /*
    * Create the custom post type
    */
    public static function create_post_type() { 
        //create the custom post type book
        register_post_type(
        'book',
        array(
            'labels'=>array(
            "name"=>__("Book"),
            "singular_name"=>__("Book")
            ),
            "public"=>true,
            "has_archive"=>true,
            'taxonomies'=>array('bookstype')
        )
        ); 


        //second, create a custom taxonomy for the custom type created
        register_taxonomy(
        'bookcat',
        "book",
        array(
            'label'=>__("WYGPBookCategories"),
            "rewrite"=>array("slug"=>"books"),
            "capabilities"=>array(
            'assign_terms'=>'edit_guides',
            'edit_terms'=>'publish_guides'
            )
        )
        ); 
    }

    //previleges
    function user_capacity() {
        //give users permissions to upload media files
        $editor = get_role("editor");
        $contributor = get_role("contributor");
        $subscriber = get_role("subscriber");
        $subscriber->add_cap("upload_files");
        $editor->add_cap("upload_files");
        $contributor->add_cap("upload_files");
    }

    /*
    *   Rendering Methods
    */

    function add_book_page() {
        return $this->get_template_html("create");
    }

    function show_books() {
        return $this->get_template_html("books");
    }

    function show_book() {
        return $this->get_template_html("book");
    }

    function show_index() {
        return $this->get_template_html("lib-index");
    }

    function add_book() {
        status_header(200);

        //get course parameters
        $book_name = $_REQUEST['book-name'];
        //$course_title = $_REQUEST['course-title'];
        $book_description = $_REQUEST['description'];
        $cover_img = $_REQUEST['image_url'];
        $book_url = $_REQUEST['pdf_url']; 
        $book_price = $_REQUEST['book-price']==''?0:$_REQUEST['book-price'];
        $book_category = $_REQUEST['book-cat']=="select"?"general":$_REQUEST['book-cat'];
        $book_fee = $_REQUEST['book-fee']=="select"?"free":$_REQUEST['book-fee'];
        $book_author = $_REQUEST['book-author'];
        global $current_user;
        $instructor = $current_user->user_login;
        //$instructor = (wp_get_current_user())->user_login;

        //use the above info to create the course post type
        $book_id = wp_insert_post(
            array(
            "post_type"=>"book",
            "post_title"=>$book_name,
            "post_content"=>$book_description,
            "post_name"=>$book_name,
            "post_status"=>"publish",
            "ping_status"=>"closed",
            "comment_status"=>"closed",
            "post_author"=>$instructor
            )
        );

        //set book category
        wp_set_object_terms($book_id,$book_category,"bookcat");

            /*
            * WRITE BOOK INFORMATION INTO LIBRARY TABLE
            */
            global $wpdb;
            //use the wp database prefix defined
            $library_table = $wpdb->prefix."wygp_library";
        //query
        $query = "INSERT INTO $library_table(book_name,book_cover,book_url,post_id,book_fee,price,book_author,book_desc)
                    VALUES('$book_name','$cover_img','$book_url','$book_id','$book_fee','$book_price','$book_author','$book_description')";

        //execute the query using the db delta function
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( $query );

        //redirect the user back to the add module page using the create course id
        wp_redirect(site_url("/books","http"));   //TODO: add alert function
    }

    function show_current_user_attachments($query) { 
        $user_id = get_current_user_id(); 
        $query['author'] = $user_id;
        return $query;
    }

    //method to perform search on the library
    function search_library() { die("seen");
       /* status_header(200);
        $search_words = '';//$_REQUEST['search-words'];
        $search_criteria = '';//$_REQUEST['criteria'];
        die("search received! $criteria, $search_words "); */
    }
}

$library = new Library();
?>